Nearlog Flutter App
===================

:Author: ColourDelete
:Version: 0.1.0-α
:License: CC BY 4.0

.. contents::


This is the docs for the Nealog *Flutter* App. Visit https://nearlog-app.readthedocs.io/en/latest/ for the Nearlog App.

Visit the repo for more info.
