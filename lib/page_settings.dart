import 'package:flutter/material.dart';

class SettingsRoute extends StatefulWidget {
  SettingsRoute({Key key, this.title}) : super(key: key);
  final String title;

  @override
  _SettingsRouteState createState() => _SettingsRouteState();
}

class _SettingsRouteState extends State<SettingsRoute>
    with WidgetsBindingObserver {
  Widget build(BuildContext context) {
    return Center(
      child: Column(
        children: <Widget>[
          SingleChildScrollView(
            child: Padding(
              padding: const EdgeInsets.all(8.0),
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                mainAxisSize: MainAxisSize.min,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  Text(
                    '⚠️ This app is work in progress. Some or all features may not be available or wrongly documented.',
                    style: Theme.of(context).textTheme.headline),
                  Divider(color: Colors.black),
//                          SettingsList(
//                            sections: [
//                              SettingsSection(
//                                title: 'Section',
//                                tiles: [
//                                  SettingsTile(
//                                    title: 'Language',
//                                    subtitle: 'English',
//                                    leading: Icon(Icons.language),
//                                    onTap: () {},
//                                  ),
////                                  SettingsTile.switchTile(
////                                    title: 'Use fingerprint',
////                                    leading: Icon(Icons.fingerprint),
////                                    switchValue: value,
////                                    onToggle: (bool value) {},
////                                  ),
////                                ],
//                              ),
//                            ],
//                          ),
                  Center(
                    child: Text('Nearlog\n',
                      style: Theme.of(context).textTheme.headline)),
                  Center(child: Text('Version 0.4.0 Alpha')),
                ]
              )
            )
          )
        ],
      )
    );
  }
}
