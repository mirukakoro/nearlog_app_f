//import 'dart:async';
//import 'dart:io';
//import 'package:flutter/material.dart';
//import 'package:flutter/services.dart';
//import 'package:flutter_beacon/flutter_beacon.dart';
//
//class ListenRoute extends StatefulWidget {
//  ListenRoute({Key key, this.title}) : super(key: key);
//  final String title;
//  @override
//  _ListenRouteState createState() => _ListenRouteState();
//}
//
//class _ListenRouteState extends State<ListenRoute>
//    with WidgetsBindingObserver {
//
//  var bleIdentifier = 'com.colourdelete.nearlog_app_f.listener'; // BLE IDENTIFIER
//  var bleProxUUID = '0ab16bb1-a576-4f2d-9be0-5a7f1bc816a7'; // BLE PROXimity UUID
//  var regions = <Region>[]; // REGIONS
//
//  bleInit() async {
//    if (Platform.isIOS) {
//      // iOS platform, at least set identifier and proximityUUID for region scanning
//      regions.add(Region(
//          identifier: bleIdentifier,
//          proximityUUID: bleProxUUID));
//    } else {
//      // Android platform, it can ranging out of beacon that filter all of Proximity UUID
//      regions.add(Region(identifier: bleIdentifier));
//    }
//    try {
//      await flutterBeacon.initializeAndCheckScanning;
//    } on PlatformException catch(e) {
//      print('ERROR: ${e}');
//    }
//  }
//
//  bleRange() async {
//    // to start ranging beacons
//    var _streamRanging = flutterBeacon.ranging(regions).listen((RangingResult result) {
//      // result contains a region and list of beacons found
//      // list can be empty if no matching beacons were found in range
//    });
//
//  // to stop ranging beacons
//    _streamRanging.cancel();
//  }
//  bleMonitor() async {
//    // to start monitoring beacons
//    var _streamMonitoring =
//        flutterBeacon.monitoring(regions).listen((MonitoringResult result) {
//          // result contains a region, event type and event state
//        });
//
//    // to stop monitoring beacons
//    _streamMonitoring.cancel();
//  }
//
//
//
//  Widget build(BuildContext context) {
//    return Scaffold(
//        appBar: AppBar(
//          title: Text('Listen'),
//        ),
//        body: Center(
//            child: Column(
//              children: <Widget>[
//                SingleChildScrollView(
//                    child: Padding(
//                        padding: const EdgeInsets.all(8.0),
//                        child: Column(
//                            mainAxisAlignment: MainAxisAlignment.center,
//                            mainAxisSize: MainAxisSize.min,
//                            crossAxisAlignment: CrossAxisAlignment.start,
//                            children: <Widget>[
//                              Text('⚠️ This app is work in progress. Some or all features may not be available or wrongly documented.',
//                                  style: Theme.of(context).textTheme.headline),
//                            ]
//                        )
//                    )
//                )
//              ],
//            )
//        )
//    );
//  }
//}

import 'dart:async';
import 'dart:io';

import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_beacon/flutter_beacon.dart';

import 'lib_file.dart';

//class ContactStorage {
//  Future<String> get _localPath async {
//    final directory = await getApplicationDocumentsDirectory();
//
//    return directory.path;
//  }
//
//  Future<File> get _localFile async {
//    final path = await _localPath;
//    return File('$path/nearlog_contact_data.txt');
//  }
//
//  Future<int> readCounter() async {
//    try {
//      final file = await _localFile;
//
//      // Read the file
//      String contents = await file.readAsString();
//
//      return int.parse(contents);
//    } catch (e) {
//      // If encountering an error, return 0
//      return 0;
//    }
//  }
//
//  Future<File> writeCounter(int counter) async {
//    final file = await _localFile;
//
//    // Write the file
//    return file.writeAsString('$counter');
//  }
//}

class ListenRoute extends StatefulWidget {
  ContactStorage storage;

  ListenRoute({Key key, @required this.storage}) : super(key: key);

  @override
  _ListenRouteState createState() => _ListenRouteState();
//  home: FlutterDemo(storage: ContactStorage()),
}

class _ListenRouteState extends State<ListenRoute> with WidgetsBindingObserver {
  StreamController<BluetoothState> streamController = StreamController();
  StreamSubscription<BluetoothState> _streamBluetooth;
  StreamSubscription<RangingResult> _streamRanging;
  var _regionBeacons = <Region, List<Beacon>>{};
  var _beacons = <Beacon>[];
  var bleIdentifier = 'com.colourdelete.nearlog_app_f'; // BLE IDENTIFIER
//  var bleProxUUID = null; // DO NOT CHANGE; WILL NOT BE ABLE TO COMMUNICATE/LISTEN TO OTHER NEARLOG STUFF // BLE PROXimity UUID
  var bleProxUUID = '00a5f9d2-7253-4535-98f1-30de9fe1d311'; // DO NOT CHANGE; WILL NOT BE ABLE TO COMMUNICATE/LISTEN TO OTHER NEARLOG STUFF // BLE PROXimity UUID
  bool authorizationStatusOk = false;
  bool locationServiceEnabled = false;
  bool bluetoothEnabled = false;
  int _counter;
  var _contacts = 'loading';
  var _dateContacts = 'date loading';

//  var _ndid = '';
  var _ndidDict = {};
  var _ndidList = [];

//  dbOpenDb() async { // DataBaseOPENDataBase
//    var database = openDatabase(
//      // Set the path to the database. Note: Using the `join` function from the
//      // `path` package is best practice to ensure the path is correctly
//      // constructed for each platform.
//      join(await getDatabasesPath(), 'nearlog_database.db'),
//      // When the database is first created, create a table to store dogs.
//      onCreate: (db, version) {
//        return db.execute(
//          "CREATE TABLE dogs(Uuid TEXT PRIMARY KEY, Date INTEGER, Duration INTEGER, MedDist INTEGER)",
//        );
//      },
//      // Set the version. This executes the onCreate function and provides a
//      // path to perform database upgrades and downgrades.
//      version: 1,
//    );
//  }

  getNDID(beacon) {
    return (beacon.major.toString() + beacon.minor.toString());
  }

  reloadContacts() async {
    var cache = await widget.storage.readContact();
    var cache2 = await widget.storage.readDateContact();
    setState(() {
      _contacts = cache;
      _dateContacts = cache2.toString();
    });
//    await clearContacts();
  }

  getDate() async {
    var now = new DateTime.now();
    return '${now.year}-${now.month}-${now.day}';
  }

  getEpoch() async {
    var now = new DateTime.now();
    return (now.millisecondsSinceEpoch / 1000).toInt();
  }

  clearContacts() async {
    await widget.storage.clearContact();
    reloadContacts();
  }

  saveContacts() async {
    await new Future.delayed(const Duration(seconds: 5));
    for (var i = 0; i < _ndidDict.length; i++) {
      var epoch = getEpoch();
      var key = _ndidDict.keys.toList()[i];
      var line = await _ndidDict[key];
      var lastTime = (line[2] + line[3]).toInt();
      var untouchedTime = (await epoch - lastTime);
      if (untouchedTime >= 5) { // 5 secs
        var duration = _ndidDict[key][3].round();
        await widget.storage.writeContact(
            '$key ${_ndidDict[key][0]} ${_ndidDict[key][1]} ${(duration / 60)
                .round()}');
        _ndidDict.remove(key);
      }
    }
//    widget.storage.clearContact();
  }

//  sortContacts() async {
//    await widget.storage.clearNonExactDuplicatesWrite(_ndidList);
//    await reloadContacts();
//  }

  getDuration(beacon) {
    var ndid = getNDID(beacon);
    var alreadyThere = _ndidDict.containsKey(ndid);
    if (alreadyThere) {
      return _ndidDict[ndid][3];
    }
    else {
      return 0;
    }
  }

  getMinDuration(beacon) {
    return (getDuration(beacon) / 60).round();
  }

  trySaveContacts(beacon) async {
    await new Future.delayed(const Duration(seconds: 5));
    var ndid = getNDID(beacon);
    var prox = beacon.proximity.toString();
    var date = getDate();
    var alreadyThere = _ndidDict.containsKey(ndid);
    var epoch = await getEpoch();
    if (alreadyThere) {
      var startTime = _ndidDict[ndid][2];
      _ndidDict[ndid] = [_ndidDict[ndid][0], _ndidDict[ndid][1],
        startTime, epoch - startTime];
    }
    else {
      _ndidDict[ndid] = [await date, prox, epoch, epoch];
      _ndidList.add(ndid);
    }
    // date prox start_time, duration
//    await widget.storage.writeContact(
//        '${await date} ${await ndid} $prox ${await epoch}');
    print('$ndid ${_ndidDict[ndid]}');
    reloadContacts();
//    print('${beacon.major} ${beacon.minor} ${beacon.accuracy} ${beacon.rssi} ${beacon.macAddress} ${beacon.proximityUUID} ${beacon.proximity}');

  }

  @override
  void initState() {
    WidgetsBinding.instance.addObserver(this);

    super.initState();
    widget.storage.readCounter().then((int value) {
      setState(() {
        _counter = value;
      });
    });
    listeningState();
//    WidgetsBinding.instance
//        .addPostFrameCallback((_) => );
  }

  listeningState() async {
    print('Listening to bluetooth state');
    _streamBluetooth = flutterBeacon
        .bluetoothStateChanged()
        .listen((BluetoothState state) async {
      print('BluetoothState = $state');
      streamController.add(state);

      switch (state) {
        case BluetoothState.stateOn:
          initScanBeacon();
          break;
        case BluetoothState.stateOff:
          await pauseScanBeacon();
          await checkAllRequirements();
          break;
      }
    });
  }

  checkAllRequirements() async {
    final bluetoothState = await flutterBeacon.bluetoothState;
    final bluetoothEnabled = bluetoothState == BluetoothState.stateOn;
    final authorizationStatus = await flutterBeacon.authorizationStatus;
    final authorizationStatusOk =
        authorizationStatus == AuthorizationStatus.allowed ||
            authorizationStatus == AuthorizationStatus.always;
    final locationServiceEnabled =
    await flutterBeacon.checkLocationServicesIfEnabled;

    setState(() {
      this.authorizationStatusOk = authorizationStatusOk;
      this.locationServiceEnabled = locationServiceEnabled;
      this.bluetoothEnabled = bluetoothEnabled;
    });
  }

  initScanBeacon() async {
    await flutterBeacon.initializeScanning;
    await checkAllRequirements();
    if (!authorizationStatusOk ||
        !locationServiceEnabled ||
        !bluetoothEnabled) {
      print('RETURNED, authorizationStatusOk=$authorizationStatusOk, '
          'locationServiceEnabled=$locationServiceEnabled, '
          'bluetoothEnabled=$bluetoothEnabled');
      return;
    }
    var regions = <Region>[
      Region(
        identifier: bleIdentifier,
        proximityUUID: bleProxUUID,
      ),
    ];

    if (_streamRanging != null) {
      if (_streamRanging.isPaused) {
        _streamRanging.resume();
        return;
      }
    }

    _streamRanging =
        flutterBeacon.ranging(regions).listen((RangingResult result) {
//          print("BLE LISTEN STUFF");
//          print(result);

          if (result != null && mounted) {
            setState(() {
              _regionBeacons[result.region] = result.beacons;
              _beacons.clear();
              _regionBeacons.values.forEach((list) {
                _beacons.addAll(list);
              });
              _beacons.sort(_compareParameters);
            });
          }
        });
  }

  pauseScanBeacon() async {
    _streamRanging?.pause();
    if (_beacons.isNotEmpty) {
      setState(() {
        _beacons.clear();
      });
    }
  }

  int _compareParameters(Beacon a, Beacon b) {
    int compare = a.proximityUUID.compareTo(b.proximityUUID);

    if (compare == 0) {
      compare = a.major.compareTo(b.major);
    }

    if (compare == 0) {
      compare = b.minor.compareTo(a.minor);
    }

    return compare;
  }

  @override
  void didChangeAppLifecycleState(AppLifecycleState state) async {
    print('AppLifecycleState = $state');
    if (state == AppLifecycleState.resumed) {
      if (_streamBluetooth != null && _streamBluetooth.isPaused) {
        _streamBluetooth.resume();
      }
      await checkAllRequirements();
      if (authorizationStatusOk && locationServiceEnabled && bluetoothEnabled) {
        await initScanBeacon();
      } else {
        await pauseScanBeacon();
        await checkAllRequirements();
      }
    } else if (state == AppLifecycleState.paused) {
      _streamBluetooth?.pause();
    }
  }

  @override
  void dispose() {
    WidgetsBinding.instance.removeObserver(this);
    streamController?.close();
    _streamRanging?.cancel();
    _streamBluetooth?.cancel();
    flutterBeacon.close;

    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Column(
      children: <Widget>[
        Center(
          child: Row(
            children: <Widget>[
              Center(
                  child: Row(
                    children: <Widget>[
                      if (!authorizationStatusOk)
                        IconButton(
                            icon: new Icon(Icons.portable_wifi_off),
                            color: Colors.red,
                            onPressed: () async {
                              await flutterBeacon.requestAuthorization;
                            }
                        )
                      else
                        IconButton(
                            icon: new Icon(Icons.wifi_tethering),
                            color: Colors.green,
                            onPressed: () async {
                              await flutterBeacon.requestAuthorization;
                            }
                        ),
                    ],
                  )
              ),
              Center(
                  child: Row(
                    children: <Widget>[
                      if (!locationServiceEnabled)
                        IconButton(
                            icon: new Icon(Icons.location_off),
                            color: Colors.red,
                            onPressed: () async {
                              await flutterBeacon.openLocationSettings;
                            }
                        )
                      else
                        IconButton(
                            icon: new Icon(Icons.location_on),
                            color: Colors.green,
                            onPressed: () async {
                              await flutterBeacon.openLocationSettings;
                            }
                        ),
                    ],
                  )
              ),
              Center(
                  child: Row(
                    children: <Widget>[
                      if (!bluetoothEnabled)
                        IconButton(
                            icon: new Icon(Icons.bluetooth_disabled),
                            color: Colors.red,
                            onPressed: () async {
                              await flutterBeacon.openBluetoothSettings;
                            }
                        )
                      else
                        IconButton(
                            icon: new Icon(Icons.bluetooth),
                            color: Colors.green,
                            onPressed: () async {
                              await flutterBeacon.openBluetoothSettings;
                            }
                        ),
                    ],
                  )
              ),
            ]
          )
        ),
//            new StreamBuilder<BluetoothState>(
//              builder: (context, snapshot) {
//                if (snapshot.hasData) {
//                  final state = snapshot.data;
//
//                  if (state == BluetoothState.stateOn) {
//                    return IconButton(
//                        icon: Icon(Icons.bluetooth_connected),
//                        onPressed: () {},
//                        color: Colors.blueAccent
//                    );
//                  }
//
//                  if (state == BluetoothState.stateOff) {
//                    return IconButton(
//                      icon: Icon(Icons.bluetooth),
//                      onPressed: () async {
//                        if (Platform.isAndroid) {
//                          try {
//                            await flutterBeacon.openBluetoothSettings;
//                          } on PlatformException catch (e) {
//                            print(e);
//                          }
//                        } else if (Platform.isIOS) {}
//                      },
//                      color: Colors.red,
//                    );
//                  }
//
//                  return IconButton(
//                    icon: Icon(Icons.bluetooth_disabled),
//                    onPressed: () {},
//                    color: Colors.grey,
//                  );
//                }
//
//                return SizedBox.shrink();
//              },
//              stream: streamController.stream.asBroadcastStream(),
//              initialData: BluetoothState.stateUnknown,
//            ),
//          ],
//        ),
        Center(
          child: Column(
              children: <Widget>[
//                      Row(
//                          children: <Widget>[
                Center(
                  child: RaisedButton(
                    onPressed: reloadContacts,
                    child: Text('Read'),
                  ),
                ),
                Center(
                  child: RaisedButton(
                    onPressed: clearContacts,
                    child: Text('Clear'),
                  ),
                ),
//                            Center(
//                              child: RaisedButton(
//                                onPressed: sortContacts,
//                                child: Text('Sort'),
//                              ),
//                            ),
//                          ]
//                      ),
                Text(
                  '$_contacts',
                ),
//                      Text(
//                        '$_dateContacts',
//                      ),
              ]
          ),
        ),
        Center(
          child: _beacons == null || _beacons.isEmpty
              ? Center(child: CircularProgressIndicator())
              : ListView(
            children: ListTile.divideTiles(
              context: context,
              tiles: _beacons.map((beacon) {
                trySaveContacts(beacon);
                saveContacts();
                return ListTile(
                  title: Text(getNDID(beacon)),
                  subtitle: new Column(
                    children: <Widget>[
//                                  Row(
//                                      children: <Widget>[
//                                        Flexible(
//                                            child: Text(
//                                                '${beacon.proximityUUID}'),
//                                            flex: 1,
//                                            fit: FlexFit.tight
//                                        ),
//                                      ]
//                                  ),
                      Row(
                        mainAxisSize: MainAxisSize.max,
                        children: <Widget>[
                          Flexible(
                              child: Text(
                                  'Duration: ${getMinDuration(
                                      beacon)} min',
                                  style: TextStyle(fontSize: 13.0)),
                              flex: 1,
                              fit: FlexFit.tight
                          ),
                          Flexible(
                              child: Text(
                                  'Accuracy: ${beacon
                                      .accuracy}m\nRSSI: ${beacon
                                      .rssi}',
                                  style: TextStyle(fontSize: 13.0)),
                              flex: 1,
                              fit: FlexFit.tight
                          ),
                        ],
                      ),
                    ]
                  )
                );
              }
            )
          ).toList(),
        ),
      ),
    ]
    );
  }
}

//// Copyright 2017, Paul DeMarco.
//// All rights reserved. Use of this source code is governed by a
//// BSD-style license that can be found in the LICENSE file.
//
//import 'dart:async';
//
//import 'package:flutter/material.dart';
//import 'package:flutter_blue/flutter_blue.dart';
//import 'widgets.dart';
//import 'package:flutter_blue_beacon/flutter_blue_beacon.dart';
//
//class ListenRoute extends StatefulWidget {
//  ListenRoute({Key key, this.title}) : super(key: key);
//
//  final String title;
//
//  @override
//  _ListenRouteState createState() => new _ListenRouteState();
//}
//
//class _ListenRouteState extends State<ListenRoute> {
//  FlutterBlueBeacon flutterBlueBeacon = FlutterBlueBeacon.instance;
//  FlutterBlue _flutterBlue = FlutterBlue.instance;
//
//  /// Scanning
//  StreamSubscription _scanSubscription;
//  Map<int, Beacon> beacons = new Map();
//  bool isScanning = false;
//
//  /// State
//  StreamSubscription _stateSubscription;
//  BluetoothState state = BluetoothState.unknown;
//
//  @override
//  void initState() {
//    super.initState();
//    // Immediately get the state of FlutterBlue
//    _flutterBlue.state.then((s) {
//      setState(() {
//        state = s;
//      });
//    });
//    // Subscribe to state changes
//    _stateSubscription = _flutterBlue.onStateChanged().listen((s) {
//      setState(() {
//        state = s;
//      });
//    });
//  }
//
//  @override
//  void dispose() {
//    _stateSubscription?.cancel();
//    _stateSubscription = null;
//    _scanSubscription?.cancel();
//    _scanSubscription = null;
//    super.dispose();
//  }
//
//  _clearAllBeacons() {
//    setState(() {
//      beacons = Map<int, Beacon>();
//    });
//  }
//
//  _startScan() {
//    print("Scanning now");
//    _scanSubscription = flutterBlueBeacon
//        .scan(timeout: const Duration(seconds: 20))
//        .listen((beacon) {
//      print('localName: ${beacon.scanResult.advertisementData.localName}');
//      print(
//          'manufacturerData: ${beacon.scanResult.advertisementData.manufacturerData}');
//      print('serviceData: ${beacon.scanResult.advertisementData.serviceData}');
//      setState(() {
//        beacons[beacon.hash] = beacon;
//      });
//    }, onDone: _stopScan);
//
//    setState(() {
//      isScanning = true;
//    });
//  }
//
//  _stopScan() {
//    print("Scan stopped");
//    _scanSubscription?.cancel();
//    _scanSubscription = null;
//    setState(() {
//      isScanning = false;
//    });
//  }
//
//  _buildScanningButton() {
//    if (state != BluetoothState.on) {
//      return null;
//    }
//    if (isScanning) {
//      return new FloatingActionButton(
//        child: new Icon(Icons.stop),
//        onPressed: _stopScan,
//        backgroundColor: Colors.red,
//      );
//    } else {
//      return new FloatingActionButton(
//          child: new Icon(Icons.search), onPressed: _startScan);
//    }
//  }
//
//  _buildScanResultTiles() {
//    return beacons.values.map<Widget>((b) {
//      if (b is IBeacon) {
//        return IBeaconCard(iBeacon: b);
//      }
//      if (b is EddystoneUID) {
//        return EddystoneUIDCard(eddystoneUID: b);
//      }
//      if (b is EddystoneEID) {
//        return EddystoneEIDCard(eddystoneEID: b);
//      }
//      return Card();
//    }).toList();
//  }
//
//  _buildAlertTile() {
//    return new Container(
//      color: Colors.redAccent,
//      child: new ListTile(
//        title: new Text(
//          'Bluetooth adapter is ${state.toString().substring(15)}',
//          style: Theme.of(context).primaryTextTheme.subhead,
//        ),
//        trailing: new Icon(
//          Icons.error,
//          color: Theme.of(context).primaryTextTheme.subhead.color,
//        ),
//      ),
//    );
//  }
//
//  _buildProgressBarTile() {
//    return new LinearProgressIndicator();
//  }
//
//  @override
//  Widget build(BuildContext context) {
//    var tiles = new List<Widget>();
//    if (state != BluetoothState.on) {
//      tiles.add(_buildAlertTile());
//    }
//
//    tiles.addAll(_buildScanResultTiles());
//
//    return new MaterialApp(
//      home: new Scaffold(
//        appBar: new AppBar(
//          title: const Text('FlutterBlueBeacon Example'),
//          actions: <Widget>[
//            IconButton(icon: Icon(Icons.clear), onPressed: _clearAllBeacons)
//          ],
//        ),
//        floatingActionButton: _buildScanningButton(),
//        body: new Stack(
//          children: <Widget>[
//            (isScanning) ? _buildProgressBarTile() : new Container(),
//            new ListView(
//              children: tiles,
//            )
//          ],
//        ),
//      ),
//    );
//  }
//}