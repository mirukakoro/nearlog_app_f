import 'dart:async';
import 'dart:io';

import 'package:path_provider/path_provider.dart';

class ContactStorage {
  Future<String> get _localPath async {
    final directory = await getApplicationDocumentsDirectory();

    return directory.path;
  }

  Future<File> get _localFile async {
    final path = await _localPath;
    return File('$path/nearlog_contact_data.txt');
  }

  Future<int> readCounter() async {
    try {
      final file = await _localFile;

      // Read the file
      String contents = await file.readAsString();

      return int.parse(contents);
    } catch (e) {
      // If encountering an error, return 0
      return 0;
    }
  }

  Future<String> readContact() async {
    try {
      final file = await _localFile;

      // Read the file
      String contents = await file.readAsString();

      return contents;
    } catch (e) {
      // If encountering an error, return 0
      return 'error';
    }
  }

  getDate() async {
    var now = new DateTime.now();
    return '${now.year}-${now.month}-${now.day}';
  }

  Future<List> readDateContact() async {
    try {
      final file = await _localFile;

      // Read the file
      String contents = await file.readAsString();

      var lines = contents.split('\n');

      for (var i = 0; i < lines.length; i++) {
        var values = lines[i].split(' ');
        var date = await getDate();
        if (values[0] != date) {
          lines.removeAt(i);
        }
      }

      return lines;
    } catch (e) {
      // If encountering an error, return 0
      return ['error', e.toString()];
    }
  }

  Future<List> readNDIDContact(ndid) async {
    try {
      final file = await _localFile;

      // Read the file
      String contents = await file.readAsString();

      var lines = contents.split('\n');

      for (var i = 0; i < lines.length; i++) {
        var values = lines[i].split(' ');
        var date = await getDate();
        if (values[0] != date && values[1] != ndid) {
          lines.removeAt(i);
        }
      }

      return lines;
    } catch (e) {
      // If encountering an error, return 0
      return ['error', e.toString()];
    }
  }

//  Future<List> clearNonExactDuplicatesWrite(ndidList) async {
//    try {
//      final file = await _localFile;
//
//      // Read the file
//      String contents = await file.readAsString();
//
//      var out = clearNonExactDuplicates(ndidList, contents);
//      await clearContact();
//      await writeContact(out);
//    }
//    catch (e) {
//      // If encountering an error, return 0
//      return ['error', e.toString()];
//    }
//  }
//
//  Future<String> clearNonExactDuplicates(ndidList, contents) async {
//    // Clears entries with the same NDID and date. Longest duration is kept.
//    try {
//      var lines = contents.split('\n');
//      var ndidMax = {};
//      for (var i = 0; i < ndidList.length; i++) {
//        for (var j = 0; j < lines.length; j++) {
//          var values = lines[j].split(' ');
//          var date = await getDate();
//          var ndid = ndidList[i];
//          if (values[0] == date && values[1] == ndid) {
//            var alreadyThere = ndidMax.containsKey(ndid);
//            if (alreadyThere) {
//              if (ndidMax[ndid][3] < values[3]) {
//                ndidMax[ndid] = values;
//              }
//            }
//            else {
//              ndidMax[ndid] = values;
//            }
//          }
//          lines.removeAt(i);
//        }
//      }
//      var out = '';
//      for (var i = 0; i < ndidMax.keys
//          .toList()
//          .length; i++) {
//        var line = ndidMax[ndidMax.keys.toList()[i]];
//        out += '${line[0]} ${line[1]} ${line[2]} ${line[3]}\n';
//      }
//      return out;
//    }
//    catch (e) {
//      // If encountering an error, return 0
//      return 'error ${e.toString()}';
//    }
//  }

  Future<File> clearContact() async {
    final file = await _localFile;

    // Write the file
    return file.writeAsString('');
  }

  Future<File> writeContact(contact) async {
    final file = await _localFile;

    String original = await readContact();

    // Write the file
    return file.writeAsString('$original\n$contact');
  }

  Future<File> writeCounter(int counter) async {
    final file = await _localFile;

    // Write the file
    return file.writeAsString('$counter');
  }
}
