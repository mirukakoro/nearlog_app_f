import 'package:flutter/material.dart';
import 'package:introduction_screen/introduction_screen.dart';
import 'package:nearlog_app_f/page_root.dart';

class IntroRoute extends StatefulWidget {
  @override
  _IntroRouteState createState() => _IntroRouteState();
}

class _IntroRouteState extends State<IntroRoute> {
  final introKey = GlobalKey<IntroductionScreenState>();

  void _onIntroEnd(context) {
    Navigator.pushReplacement(
        context,
        MaterialPageRoute(
            builder: (context) =>
            new RootRoute(
              title: 'Nearlog',
            )
        )
    );
  }

  Widget _buildImage(String assetName, String assetExt) {
    return Align(
      child: Image.asset('assets/$assetName.$assetExt', width: 350.0),
      alignment: Alignment.bottomCenter,
    );
  }

  @override
  Widget build(BuildContext context) {
    const bodyStyle = TextStyle(fontSize: 19.0);
    const pageDecoration = const PageDecoration(
      titleTextStyle: TextStyle(fontSize: 28.0, fontWeight: FontWeight.w700),
      bodyTextStyle: bodyStyle,
      descriptionPadding: EdgeInsets.fromLTRB(16.0, 0.0, 16.0, 16.0),
//      pageColor: Colors.white,
      imagePadding: EdgeInsets.zero,
    );

    return IntroductionScreen(
      key: introKey,
      pages: [
        PageViewModel(
          title: "⚠️Warning: Work In Progress",
          body:
              "This application/serviceis in work in progress. Information may be incorrect.",
//          image: _buildImage('img1'),
          decoration: pageDecoration,
        ),
        PageViewModel(
          title: 'Get Started',
          bodyWidget: Column(
            mainAxisAlignment: MainAxisAlignment.start,
            children: const [
              Text('1. Make sure all icons are green.', style: bodyStyle),
              Divider(),
              Text('2. Allow permissions', style: bodyStyle),
            ],
          ),
//          image: _buildImage('img2'),
          decoration: pageDecoration,
          footer: Column(
            children: <Widget>[
              RaisedButton(
                onPressed: () {
                  introKey.currentState?.animateScroll(0);
                },
                child: const Text(
                  'Learn More',
                ),
              ),
              Text(
                '(opens https://nearlog.gitlab.io)',
              ),
            ],
          ),
        ),
      ],
      onDone: () => _onIntroEnd(context),
      onSkip: () => _onIntroEnd(context),
      // You can override onSkip callback
      showSkipButton: true,
      skipFlex: 0,
      nextFlex: 0,
      skip: const Icon(Icons.skip_next),
      next: const Icon(Icons.arrow_forward),
      done: const Icon(Icons.done),
      dotsDecorator: const DotsDecorator(
        size: Size(10.0, 10.0),
        color: Color(0xFFBDBDBD),
        activeSize: Size(20.0, 10.0),
        activeShape: RoundedRectangleBorder(
          borderRadius: BorderRadius.all(Radius.circular(25.0)),
        ),
      ),
    );
  }
}

class HomePage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(title: const Text('Home')),
      body: const Center(child: Text("This is the screen after Introduction")),
    );
  }
}
