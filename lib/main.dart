import 'dart:async';
import 'dart:ui';

import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:nearlog_app_f/page_intro.dart';
import 'package:nearlog_app_f/page_root.dart';
import 'package:package_info/package_info.dart';
import 'package:shared_preferences/shared_preferences.dart';

void main() {
  runApp(RootApp());
}

class RootApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
//    return MaterialApp(
//      title: 'Introduction screen',
//      debugShowCheckedModeBanner: false,
//      theme: ThemeData(primarySwatch: Colors.blue),
//      home: OnBoardingPage(),
//    );
    return MaterialApp(
      title: 'Nearlog',
      theme: ThemeData(
        brightness: Brightness.light,
        primaryColor: Colors.white,
      ),
      darkTheme: ThemeData(
        brightness: Brightness.dark,
        primaryColor: Colors.blue,
      ),
      home: SafeArea(
        child: Splash(),
      ),
    );
  }
}

class Splash extends StatefulWidget {
  @override
  _SplashState createState() => new _SplashState();
}

class _SplashState extends State<Splash> with WidgetsBindingObserver {
  pushRoot() {
    Navigator.pushReplacement(
        context,
        MaterialPageRoute(
            builder: (context) =>
            new RootRoute(
              title: 'Nearlog',
            )
        )
    );
  }

  pushIntro(SharedPreferences prefs) async {
    prefs.setBool('first_start', true);
    Navigator.of(context).pushReplacement(
        new MaterialPageRoute(
            builder: (context) => new IntroRoute()
        )
    ).then(
            (_) {
          pushRoot();
        }
    );
  }

  Future checkFirstSeen() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    bool _seen = (prefs.getBool('first_start') ?? false);
    if (_seen) {
      pushRoot();
    } else {
      pushIntro(prefs);
    }
  }

  @override
  void initState() {
    super.initState();
    new Timer(new Duration(milliseconds: 200), () {
      checkFirstSeen();
    });
  }

  Future<List<String>> initInfo() async {
    final info = await getInfo();
    return info;
  }

  Future<List<String>> getInfo() async {
    PackageInfo packageInfo = await PackageInfo.fromPlatform();
    List<String> result = new List(5);
    result[0] = packageInfo.appName;
    result[1] = packageInfo.packageName;
    result[2] = packageInfo.version;
    result[3] = packageInfo.buildNumber;
    return result;
  }

  @override
  Widget build(BuildContext context) {
    return new Scaffold(
      body: new Center(
        child: Builder(
          builder: (context) =>
              Center(
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: <Widget>[
                    CircularProgressIndicator(),
                    Text('\nLoading...'),
                  ],
                ),
              ),
        ),
      ),
    );
  }
}