//import 'package:flutter/material.dart';
//import 'package:uuid/uuid.dart';
//import 'package:shared_preferences/shared_preferences.dart';
//
//class HistoryRoute extends StatefulWidget {
//  HistoryRoute({Key key, this.title}) : super(key: key);
//
//  // This widget is the home page of your application. It is stateful, meaning
//  // that it has a State object (defined below) that contains fields that affect
//  // how it looks.
//
//  // This class is the configuration for the state. It holds the values (in this
//  // case the title) provided by the parent (in this case the App widget) and
//  // used by the build method of the State. Fields in a Widget subclass are
//  // always marked "final".
//
//  final String title;
//
//  @override
//  _HistoryRouteState createState() => _HistoryRouteState();
//}
//
//class _HistoryRouteState extends State<HistoryRoute> {
//  var uuid = new Uuid();
//  var _uuid = "not_set";
//  Widget build(BuildContext context) {
//    return Scaffold(
//        appBar: AppBar(
//          title: Text('Saving data'),
//        ),
//        body: Column(
//          //mainAxisAlignment: MainAxisAlignment.center,
//          children: <Widget>[
//            Padding(
//              padding: const EdgeInsets.all(8.0),
//              child: RaisedButton(
//                child: Text('Read'),
//                onPressed: () {
//                  _readUuid();
//                },
//              ),
//            ),
//            Padding(
//              padding: const EdgeInsets.all(8.0),
//              child: RaisedButton(
//                child: Text('Save'),
//                onPressed: () {
//                  _saveUuid();
//                },
//              ),
//            ),
//            Padding(
//              padding: const EdgeInsets.all(8.0),
//              child: RaisedButton(
//                child: Text('Gen'),
//                onPressed: () {
//                  _genUuid();
//                },
//              ),
//            ),
//            Padding(
//              padding: const EdgeInsets.all(8.0),
//              child: RaisedButton(
//                child: Text('GenSave'),
//                onPressed: () {
//                  _genSaveUuid();
//                },
//              ),
//            ),
//            Padding(
//              padding: const EdgeInsets.all(8.0),
//              child: Text('$_uuid'),
//            )
//          ],
//        ));
//  }
//
//  // Replace these two methods in the examples that follow
//
//  _readUuid() async {
//    final prefs = await SharedPreferences.getInstance();
//    final key = 'uuid';
//    final value = prefs.getString(key) ?? 0;
//    setState(() { _uuid = value; });
//    print('read: $value');
//  }
//
//  _saveUuid() async {
//    final prefs = await SharedPreferences.getInstance();
//    final key = 'uuid';
//    final value = '$_uuid';
//    prefs.setString(key, value);
//    print('saved $value');
//    print('uuid is now $_uuid');
//  }
//
//  _genUuid() async {
//    var newValue = uuid.v4();
//    setState(() { _uuid = newValue; });
//  }
//
//  _genSaveUuid() async {
//    await _genUuid();
//    _saveUuid();
//  }
//}
