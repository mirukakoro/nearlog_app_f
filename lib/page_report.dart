import 'package:flutter/material.dart';

class ReportRoute extends StatefulWidget {
  ReportRoute({Key key, this.title}) : super(key: key);
  final String title;
  @override
  _ReportRouteState createState() => _ReportRouteState();
}

class _ReportRouteState extends State<ReportRoute> with WidgetsBindingObserver {
  Widget build(BuildContext context) {
    return Center(
      child: Column(
        children: <Widget>[
          SingleChildScrollView(
            child: Padding(
              padding: const EdgeInsets.all(8.0),
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                mainAxisSize: MainAxisSize.min,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  Text('⚠️ This app is work in progress. Some or all features may not be available or wrongly documented.',
                    style: Theme.of(context).textTheme.headline),
                  Divider(color: Colors.black),
                  Center(
                    child: RaisedButton(
                      onPressed: () {
                        print('positive report');
                      },
                      child: Text('I tested positive'),
                    ),
                  ),
                  Center(
                    child: RaisedButton(
                      onPressed: () {
                        print('negative report');
                      },
                      child: Text('I tested negative'),
                    ),
                  ),
                ],
              )
            )
          )
        ],
      )
    );
  }
}
